/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package floppypenguin;

import java.awt.Graphics;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Robert
 */
public class FloppyPenguin extends JFrame implements KeyListener{

    private double peny = 400;
    private double penyVel = 0;
    private double penyAc = -.0000000000025;
    private int score;
    private static final int gapDistance = 100;
    private ArrayList<int[]> bergs = new ArrayList<int[]>();
    private ArrayList<int[]> sharks = new ArrayList<int[]>();
    private BufferedImage bg, pen, ice, ice2, shark;
    private int bg1x = 0, bg1y = 0, bg2x = 0, bg2y = 0;
    private int frameCount = 0, bergCounter = 0, sharkCounter = 0;
    
    private boolean spacePressed = false;
    
    public FloppyPenguin(){
        super("FloppyPenguin");
        this.setIconImage(null);
        this.requestFocusInWindow();
        this.setSize(500,500);
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.addKeyListener(this);
        this.setVisible(true);
        this.setIconImage(new ImageIcon(getClass().getResource("penguin.png")).getImage());
        
        //read images
        try {
            BufferedImage temp;
            
            
            temp = ImageIO.read(getClass().getResource("background.png"));
            BufferedImage temp2 = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);
            temp2.getGraphics().drawImage(temp.getScaledInstance(this.getWidth(), this.getHeight(),  java.awt.Image.SCALE_SMOOTH),0,0,this);
            bg = temp2;  
            
            temp = ImageIO.read(getClass().getResource("penguin.png"));
            temp2 = new BufferedImage(80, 20, BufferedImage.TYPE_INT_ARGB);
            temp2.getGraphics().drawImage(temp.getScaledInstance(80, 20,  java.awt.Image.SCALE_SMOOTH),0,0,this);
            pen = temp2;
            
            temp = ImageIO.read(getClass().getResource("ice.png"));
            temp2 = new BufferedImage(200, this.getHeight(), BufferedImage.TYPE_INT_ARGB);
            temp2.getGraphics().drawImage(temp.getScaledInstance(200, this.getHeight(),  java.awt.Image.SCALE_SMOOTH),0,0,this);
            ice = temp2;
            
            temp = ImageIO.read(getClass().getResource("ice2.png"));
            temp2 = new BufferedImage(200, this.getHeight(), BufferedImage.TYPE_INT_ARGB);
            temp2.getGraphics().drawImage(temp.getScaledInstance(200, this.getHeight(),  java.awt.Image.SCALE_SMOOTH),0,0,this);
            ice2 = temp2;
            
            temp = ImageIO.read(getClass().getResource("shark.png"));
            temp2 = new BufferedImage(240, 60, BufferedImage.TYPE_INT_ARGB);
            temp2.getGraphics().drawImage(temp.getScaledInstance(240, 60,  java.awt.Image.SCALE_SMOOTH),0,0,this);
            shark = temp2;
            
        } catch (IOException ex) {
            System.out.println("One or more of the images had a problem!");
        }
        
        while(true){
            this.repaint();
            this.updatePos();
            this.collisionCheck();
        }
    }
    
    public static void main(String[] args) {new FloppyPenguin();}
    
    private void collisionCheck() {
        if( peny > this.getHeight()*4/5){
            int ans = JOptionPane.showConfirmDialog(this, "GAME OVER! Score: " + score +". Try Agian?", "GAME OVER", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, new ImageIcon(pen));
            if (ans == 0){
                peny = 400;
                penyVel = 0;
                score = 0;
                bergs.clear();
                sharks.clear();
            }else{
                System.exit(0);
            }
        }
        
        for(int i = 0; i<bergs.size(); i++){
            if(bergs.get(i)[0]<-200){
                for(int j = i; j<bergs.size(); j++){
                    if(j+1 == bergs.size()){
                        bergs.remove(j);
                        break;
                    }
                    bergs.set(j, bergs.get(j+1));
                }
            }
            if(bergs.get(i)[0] >=  100 && bergs.get(i)[0] <=  180){
                if(peny+20 >= bergs.get(i)[1] || peny <= bergs.get(i)[1] - gapDistance){
                    int ans = JOptionPane.showConfirmDialog(this, "GAME OVER! Score: " + score +". Try Agian?", "GAME OVER", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, new ImageIcon(pen));
                    if (ans == 0){
                        peny = 400;
                        penyVel = 0;
                        score = 0;
                        bergs.clear();
                        sharks.clear();
                    }else{
                        System.exit(0);
                    }
                } else {
                }
            }
        }
        
        for(int i = 0; i<sharks.size(); i++){
            if(sharks.get(i)[0]<-240){
                for(int j = i; j<sharks.size(); j++){
                    if(j+1 == sharks.size()){
                        sharks.remove(j);
                        break;
                    }
                    sharks.set(j, sharks.get(j+1));
                }
            }
        }
        
    }
    
    private void updatePos() {
    	if(spacePressed) {
            penyVel-=.00000000001;
    	}
        if(frameCount > 50000){
           bg1x--; 
           frameCount = 0;
           if(bg1x<this.getWidth()*-1){
               bg1x = 0;
           }
           score ++;
           for(int i = 0; i<bergs.size(); i++){
               bergs.get(i)[0]--;
           }
           for(int i = 0; i<sharks.size(); i++){
               sharks.get(i)[0]--;
           }
        }
        frameCount++;
        bg2x = this.getWidth() + bg1x;
        
        penyVel -= penyAc;
        
        peny += penyVel;
        
        if(peny < 0){
            peny = 0;
            penyVel = 0;
        }
        
        if(bergCounter > 50000000){
            int[] temp = {this.getWidth(), (int)(Math.random()*(this.getHeight()*4/5) + 50)};
            bergs.add(temp);
            bergCounter = 0;
        }
        bergCounter++;
        
        if(Math.random()*50000000<1){
            int[] temp = {this.getWidth(), (int)(Math.random()*(this.getHeight()*4/5) + 50)};
            sharks.add(temp);
        }
        
        this.repaint();
        
    }
    
    @Override
    public void paint (Graphics g){
        //super.paint(g);
        BufferedImage bi = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics b = bi.getGraphics();
        
        //draw Backround
        b.drawImage(bg, bg1x, bg1y, this);
        b.drawImage(bg, bg2x, bg2y, this);
        
        //draw penguin
        
        //rotate panguin
        try{
            double rotationRequired = Math.toRadians(penyVel*450000);
            double locationX = pen.getWidth()/2;
            double locationY = pen.getHeight()/2;
            AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

            // Drawing the rotated image at the required drawing locations
            b.drawImage(op.filter(pen, null), 200, (int)peny, null);
        }catch(Exception ex){}
        //b.drawImage(pen, 200, (int)peny, this);
        
        //draw ice
        try{
            for(int i = 0; i<bergs.size(); i++){
                b.drawImage(ice, bergs.get(i)[0], bergs.get(i)[1], this);
                b.drawImage(ice2, bergs.get(i)[0], bergs.get(i)[1]-this.getHeight()-gapDistance, this);
            }
        }catch(Exception ex){}
        
        //draw sharks
        try{
            for(int i = 0; i<sharks.size(); i++){
                b.drawImage(shark, sharks.get(i)[0], sharks.get(i)[1], this);
            }
        }catch(Exception ex){}
        
        b.setColor(Color.CYAN);
        b.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 100));
        b.drawString("Score: " + score, 0, 100);
        
        g.drawImage(bi, 0, 0, this);
    }
    
    @Override
    public void keyTyped(KeyEvent ke) {}

    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_SPACE){
        	spacePressed = true;
        }
    }
    @Override
    public void keyReleased(KeyEvent ke) {
    	if(ke.getKeyCode() == KeyEvent.VK_SPACE){
        	spacePressed = false;
        }
    }
}